from flask import Flask, request, jsonify
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQL_ALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:root@localhost/test_db'
app.config['SQLAlchemy_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
ma = Marshmallow(app)

class Task(db.Model):
    id_vehiculo = db.Column(db.Integer, primary_key=True)
    matricula = db.Column(db.String(10), unique=True)
    id_estado = db.Column(db.String(2))

    def __init__(self, id_vehiculo, matricula, id_estado):
        self.id_vehiculo = id_vehiculo
        self.matricula = matricula
        self.id_estado = id_estado

db.create_all()

class TaskSchema(ma.Schema):
    class Meta:
        fields = ('id_vehiculo', 'matricula', 'id_estado')

task_schema= TaskSchema()
tasks_schema=TaskSchema(many=True)

@app.route('/vehiculos', methods=['GET'])
def get_vehiculo():
    all_tasks = Task.query.all()
    result = task_schema.dump(all_tasks)
    return jsonify(result)

@app.route('/vehiculos/<matricula>', methods=['GET'])
def get_vehiculoestado(matricula):
    task = Task.query.get(matricula)
    return task_schema.jsonify(task)

@app.route('/addvehiculos', methods=['POST'])
def add_vehiculo():
    id_coche = request.json['id_vehiculo']
    id_matricula = request.json['matricula']
    estaod_id = request.json['id_estado']

    new_task = Task(id_coche, id_matricula, estaod_id)
    
    db.session.add(new_task)
    db.session.commit()
    
    return task_schema.jsonify(new_task)

if __name__ == "__main__":
    app.run(debug=True)